package com.example.augardevent.Views;

import android.net.Uri;
import android.os.Bundle;

import com.example.augardevent.R;
import com.example.augardevent.Views.Fragments.MyRafflesFragment;
import com.example.augardevent.Views.Fragments.RafflesFragment;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.example.augardevent.Views.ui.main.SectionsPagerAdapter;

public class TabbedRafflesActivity extends AppCompatActivity implements RafflesFragment.OnFragmentInteractionListener, MyRafflesFragment.OnFragmentInteractionListener {


    private SectionsPagerAdapter mSectionPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed_raffles);
        mSectionPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(mSectionPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}


