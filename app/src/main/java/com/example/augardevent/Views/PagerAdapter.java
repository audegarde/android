package com.example.augardevent.Views;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.augardevent.Views.Fragments.MyRafflesFragment;
import com.example.augardevent.Views.Fragments.RafflesFragment;

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    RafflesFragment tab1 = new RafflesFragment();
                    return tab1;
                case 1:
                    MyRafflesFragment tab2 = new MyRafflesFragment();
                    return tab2;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

