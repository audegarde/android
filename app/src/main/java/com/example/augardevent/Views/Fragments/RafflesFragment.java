package com.example.augardevent.Views.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.augardevent.Adapter.RaffleAdapter;
import com.example.augardevent.Model.DTO.RafflesDTO;
import com.example.augardevent.R;
import com.example.augardevent.Service.Interface.UserService;
import com.example.augardevent.Service.Provider.NetworkProvider;
import com.example.augardevent.Views.RaffleDetailsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RafflesFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private RaffleAdapter raffleAdapter;

    public RafflesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_raffles, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_raffles_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        raffleAdapter = new RaffleAdapter();
        recyclerView.setAdapter(raffleAdapter);
        raffleAdapter.setItemClickListener(rafflesDTO -> {
            Intent i = new Intent(getActivity(), RaffleDetailsActivity.class);
            i.putExtra("id", rafflesDTO.getId());
            startActivity(i);
        });
        loadData();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void loadData() {
        NetworkProvider.getInstance().getRafflesList(new NetworkProvider.Listener<List<RafflesDTO>>() {
            @Override
            public void onSuccess(List<RafflesDTO> data) {
                if (data.size() > 0) {
                    raffleAdapter.setRafflesList(data);
                }
            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }
}
