package com.example.augardevent.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.augardevent.Model.DTO.SessionDTO;
import com.example.augardevent.Model.DTO.SignupDTO;
import com.example.augardevent.Model.Session;
import com.example.augardevent.R;
import com.example.augardevent.Service.Interface.UserService;
import com.example.augardevent.Service.Provider.NetworkProvider;
import com.example.augardevent.SharedPreferences.SharedPref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.signup_activity_email_edit_text)
    EditText emailET;
    @BindView(R.id.signup_activity_first_name_edit_text)
    EditText firstNameET;
    @BindView(R.id.signup_activity_last_name_edit_text)
    EditText lastNameET;
    @BindView(R.id.signup_activity_password_edit_text)
    EditText passwordET;
    @BindView(R.id.signup_activity_submit_button)
    Button submitButton;
    @BindView(R.id.signup_activity_gender_radio_group)
    RadioGroup genderGroup;
    private String gender = "";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        sharedPreferences = SharedPref.getInstance(this);
        genderGroup.setOnCheckedChangeListener(((group, checkedId) -> {
            switch(checkedId) {
                case R.id.signup_activity_female_radio_button:
                    gender = "F";
                    break;
                case R.id.signup_activity_male_radio_button:
                    gender = "M";
                    break;
                case R.id.signup_activity_other_radio_button:
                    gender = "O";
                    break;
            }
        }));
    }


    @OnClick(R.id.signup_activity_submit_button)
    void signup() {
        UserService userService;
        userService = NetworkProvider.getClient().create(UserService.class);
        SignupDTO signupDTO = new SignupDTO(
                emailET.getText().toString(),
                firstNameET.getText().toString(),
                lastNameET.getText().toString(),
                passwordET.getText().toString(),
                gender);
        Call<SessionDTO> sessionDTOCall = userService.signup(signupDTO);
        sessionDTOCall.enqueue(new Callback<SessionDTO>() {
            @Override
            public void onResponse(Call<SessionDTO> call, Response<SessionDTO> response) {
                if(response.isSuccessful()) {
                    Log.d("toz", "CA MARCHE2");
                    SessionDTO sessionDTO = response.body();
                    sharedPreferences.edit().putString("token", sessionDTO.getToken()).apply();
                    Intent i = new Intent(SignupActivity.this, TabbedRafflesActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void onFailure(Call<SessionDTO> call, Throwable t) {
            }
        });
    }
}
