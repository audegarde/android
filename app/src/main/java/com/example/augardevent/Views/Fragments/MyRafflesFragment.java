package com.example.augardevent.Views.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.augardevent.Adapter.RaffleAdapter;
import com.example.augardevent.Model.DTO.RafflesDTO;
import com.example.augardevent.R;
import com.example.augardevent.Service.Provider.NetworkProvider;
import com.example.augardevent.Views.RaffleDetailsActivity;

import java.util.List;

public class MyRafflesFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private RaffleAdapter raffleAdapter;

    public MyRafflesFragment() {
        // Required empty public constructor
    }

    public static MyRafflesFragment newInstance(String param1, String param2) {
        MyRafflesFragment fragment = new MyRafflesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_raffles, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_my_raffles_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        raffleAdapter = new RaffleAdapter();
        recyclerView.setAdapter(raffleAdapter);
        raffleAdapter.setItemClickListener(rafflesDTO -> {
            Intent i = new Intent(getActivity(), RaffleDetailsActivity.class);
            i.putExtra("id", rafflesDTO.getId());
            startActivity(i);
        });
        loadData();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void loadData() {
        NetworkProvider.getInstance().getMyRafflesList(new NetworkProvider.Listener<List<RafflesDTO>>() {
            @Override
            public void onSuccess(List<RafflesDTO> data) {
                if (data.size() > 0) {
                    raffleAdapter.setRafflesList(data);
                }
            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }
}
