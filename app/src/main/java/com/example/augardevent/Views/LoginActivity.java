package com.example.augardevent.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.example.augardevent.Model.DTO.LoginDTO;
import com.example.augardevent.Model.DTO.SessionDTO;
import com.example.augardevent.R;
import com.example.augardevent.Service.Interface.UserService;
import com.example.augardevent.Service.Provider.NetworkProvider;
import com.example.augardevent.SharedPreferences.SharedPref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.login_activity_email_edit_text)
    EditText emailET;

    @BindView(R.id.login_activity_password_edit_text)
    EditText passwordET;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        sharedPreferences = SharedPref.getInstance(this);
    }

    @OnClick(R.id.login_activity_submit_button)
    void login() {
        UserService userService;
        userService = NetworkProvider.getClient().create(UserService.class);
        LoginDTO loginDTO = new LoginDTO(emailET.getText().toString(), passwordET.getText().toString());

        Call<SessionDTO> sessionDTOCall = userService.login(loginDTO);
        sessionDTOCall.enqueue(new Callback<SessionDTO>() {
            @Override
            public void onResponse(Call<SessionDTO> call, Response<SessionDTO> response) {
                if(response.isSuccessful()) {
                    SessionDTO sessionDTO = response.body();
                    sharedPreferences.edit().putString("token", sessionDTO.getToken()).apply();
                    Intent i = new Intent(LoginActivity.this, TabbedRafflesActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void onFailure(Call<SessionDTO> call, Throwable t) {
                Log.e("ERR", "Services", t);
            }
        });

    }
}
