package com.example.augardevent.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.augardevent.Model.DTO.RafflesDTO;
import com.example.augardevent.Model.DTO.RafflesListDTO;
import com.example.augardevent.R;
import com.example.augardevent.Service.Interface.RafflesService;
import com.example.augardevent.Service.Provider.NetworkProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RaffleDetailsActivity extends AppCompatActivity {

    @BindView(R.id.fragment_raffles_details_main_image_view)
    ImageView mainImageView;
    @BindView(R.id.fragment_raffles_details_image1_image_view)
    ImageView imageView1;
    @BindView(R.id.fragment_raffles_details_image2_image_view)
    ImageView imageView2;
    @BindView(R.id.fragment_raffles_details_image3_image_view)
    ImageView imageView3;
    @BindView(R.id.fragment_raffles_details_image4_image_view)
    ImageView imageView4;
    @BindView(R.id.fragment_raffles_details_name_text_view)
    TextView nameTextView;
    @BindView(R.id.fragment_raffles_details_description_text_view)
    TextView descriptionTextView;
    @BindView(R.id.fragment_raffles_details_timer_text_view)
    TextView timerTextView;
    @BindView(R.id.fragment_raffles_details_price_text_view)
    TextView priceTextView;
    private Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raffle_details);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("id");
        RafflesService rafflesService;
        rafflesService = NetworkProvider.getClient().create(RafflesService.class);
        Call<RafflesListDTO> rafflesDTOCall = rafflesService.getOneRaffle(id.toString());
        rafflesDTOCall.enqueue(new Callback<RafflesListDTO>() {
            @Override
            public void onResponse(Call<RafflesListDTO> call, Response<RafflesListDTO> response) {
                if(response.isSuccessful()) {
                    RafflesDTO rafflesDTO = response.body().getRafflesDTO();
                    Glide.with(RaffleDetailsActivity.this).load(rafflesDTO.getWatchDTO().getPictureDTOArrayList().get(0).getUrl()).into(mainImageView);
                    Glide.with(RaffleDetailsActivity.this).load(rafflesDTO.getWatchDTO().getPictureDTOArrayList().get(1).getUrl()).into(imageView1);
                    Glide.with(RaffleDetailsActivity.this).load(rafflesDTO.getWatchDTO().getPictureDTOArrayList().get(2).getUrl()).into(imageView2);
                    Glide.with(RaffleDetailsActivity.this).load(rafflesDTO.getWatchDTO().getPictureDTOArrayList().get(3).getUrl()).into(imageView3);
                    Glide.with(RaffleDetailsActivity.this).load(rafflesDTO.getWatchDTO().getPictureDTOArrayList().get(4).getUrl()).into(imageView4);
                    nameTextView.setText(rafflesDTO.getWatchDTO().getName());
                    descriptionTextView.setText(rafflesDTO.getWatchDTO().getDescriptionDTOArrayList().get(0).getValue());
                    new CountDownTimer(Integer.valueOf(rafflesDTO.getEndTime()), 1000) {

                        public void onTick(long millisUntilFinished) {
                            timerTextView.setText("seconds remaining: " + millisUntilFinished / 1000);
                        }

                        public void onFinish() {
                            timerTextView.setText("Done!");
                        }
                    }.start();
                    priceTextView.setText(rafflesDTO.getWatchDTO().getPrice().toString());
                }
            }

            @Override
            public void onFailure(Call<RafflesListDTO> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.fragment_raffles_details_subscribe_button)
    void subscribe() {

    }


}
