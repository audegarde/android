package com.example.augardevent.Model.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WatchDTO {

    @SerializedName("id")
    Integer id;
    @SerializedName("name")
    String name;
    @SerializedName("price")
    Float price;
    @SerializedName("createdAt")
    String createdAt;
    @SerializedName("updatedAt")
    String updatedAt;
    @SerializedName("deletedAt")
    String deletedAt;
    @SerializedName("descriptions")
    ArrayList<DescriptionDTO> descriptionDTOArrayList;
    @SerializedName("picture")
    ArrayList<PictureDTO> pictureDTOArrayList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public ArrayList<DescriptionDTO> getDescriptionDTOArrayList() {
        return descriptionDTOArrayList;
    }

    public void setDescriptionDTOArrayList(ArrayList<DescriptionDTO> descriptionDTOArrayList) {
        this.descriptionDTOArrayList = descriptionDTOArrayList;
    }

    public ArrayList<PictureDTO> getPictureDTOArrayList() {
        return pictureDTOArrayList;
    }

    public void setPictureDTOArrayList(ArrayList<PictureDTO> pictureDTOArrayList) {
        this.pictureDTOArrayList = pictureDTOArrayList;
    }

    public WatchDTO(Integer id, String name, Float price, String createdAt, String updatedAt, String deletedAt, ArrayList<DescriptionDTO> descriptionDTOArrayList, ArrayList<PictureDTO> pictureDTOArrayList) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.descriptionDTOArrayList = descriptionDTOArrayList;
        this.pictureDTOArrayList = pictureDTOArrayList;
    }

    @Override
    public String toString() {
        return "WatchDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", deletedAt='" + deletedAt + '\'' +
                ", descriptionDTOArrayList=" + descriptionDTOArrayList +
                ", pictureDTOArrayList=" + pictureDTOArrayList +
                '}';
    }
}
