package com.example.augardevent.Model;

public class Session {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Session(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Session{" +
                "token='" + token + '\'' +
                '}';
    }
}
