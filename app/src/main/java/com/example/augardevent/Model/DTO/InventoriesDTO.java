package com.example.augardevent.Model.DTO;

import com.google.gson.annotations.SerializedName;

public class InventoriesDTO {

    @SerializedName("size")
    SizeDTO sizeDTO;
    @SerializedName("quantity")
    Integer quantity;

    public SizeDTO getSizeDTO() {
        return sizeDTO;
    }

    public void setSizeDTO(SizeDTO sizeDTO) {
        this.sizeDTO = sizeDTO;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public InventoriesDTO(SizeDTO sizeDTO, Integer quantity) {
        this.sizeDTO = sizeDTO;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "InventoriesDTO{" +
                "sizeDTO=" + sizeDTO +
                ", quantity=" + quantity +
                '}';
    }
}
