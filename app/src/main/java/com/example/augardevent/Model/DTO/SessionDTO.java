package com.example.augardevent.Model.DTO;

import com.google.gson.annotations.SerializedName;

public class SessionDTO {

    @SerializedName("token")
    String token;

    public String getToken() {
        return token;
    }

    public SessionDTO(String token) {
        this.token = token;
    }
}
