package com.example.augardevent.Model.DTO;

import com.google.gson.annotations.SerializedName;

public class LoginDTO {

    @SerializedName("email")
    String email;
    @SerializedName("pwd")
    String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginDTO{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public LoginDTO(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
