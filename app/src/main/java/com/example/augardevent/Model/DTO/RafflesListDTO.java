package com.example.augardevent.Model.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RafflesListDTO {

    @SerializedName("raffles")
    private List<RafflesDTO> rafflesDTOList;
    @SerializedName("raffle")
    private RafflesDTO rafflesDTO;

    public RafflesDTO getRafflesDTO() {
        return rafflesDTO;
    }

    public List<RafflesDTO> getRafflesDTOList() {
        return rafflesDTOList;
    }
}
