package com.example.augardevent.Model.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RafflesDTO {
    @SerializedName("id")
    Integer id;
    @SerializedName("startTime")
    String startTime;
    @SerializedName("endTime")
    String endTime;
    @SerializedName("product")
    WatchDTO watchDTO;
    @SerializedName("inventories")
    ArrayList<InventoriesDTO> inventoriesDTO;
    @SerializedName("createdAt")
    String createdAt;
    @SerializedName("updatedAt")
    String updatedAt;
    @SerializedName("deletedAt")
    String deletedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public WatchDTO getWatchDTO() {
        return watchDTO;
    }

    public void setWatchDTO(WatchDTO watchDTO) {
        this.watchDTO = watchDTO;
    }

    public ArrayList<InventoriesDTO> getInventoriesDTO() {
        return inventoriesDTO;
    }

    public void setInventoriesDTO(ArrayList<InventoriesDTO> inventoriesDTO) {
        this.inventoriesDTO = inventoriesDTO;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public RafflesDTO(Integer id, String startTime, String endTime, WatchDTO watchDTO, ArrayList<InventoriesDTO> inventoriesDTO, String createdAt, String updatedAt, String deletedAt) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.watchDTO = watchDTO;
        this.inventoriesDTO = inventoriesDTO;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    @Override
    public String toString() {
        return "RafflesDTO{" +
                "id=" + id +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", watchDTO=" + watchDTO +
                ", inventoriesDTO=" + inventoriesDTO +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", deletedAt='" + deletedAt + '\'' +
                '}';
    }
}
