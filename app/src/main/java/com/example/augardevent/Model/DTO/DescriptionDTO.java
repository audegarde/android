package com.example.augardevent.Model.DTO;

import com.google.gson.annotations.SerializedName;

public class DescriptionDTO {

    @SerializedName("language") String language;
    @SerializedName("value") String value;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DescriptionDTO(String language, String value) {
        this.language = language;
        this.value = value;
    }

    @Override
    public String toString() {
        return "DescriptionDTO{" +
                "language='" + language + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
