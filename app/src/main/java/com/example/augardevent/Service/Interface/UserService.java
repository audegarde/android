package com.example.augardevent.Service.Interface;

import com.example.augardevent.Model.DTO.LoginDTO;
import com.example.augardevent.Model.DTO.SessionDTO;
import com.example.augardevent.Model.DTO.SignupDTO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {

    @POST("clients/register")
    Call<SessionDTO> signup(@Body SignupDTO signupDTO);

    @POST("clients/login")
    Call<SessionDTO> login(@Body LoginDTO loginDTO);
}
