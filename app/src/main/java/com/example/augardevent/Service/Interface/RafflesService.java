package com.example.augardevent.Service.Interface;

import com.example.augardevent.Model.DTO.RafflesDTO;
import com.example.augardevent.Model.DTO.RafflesListDTO;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface RafflesService {

    @GET("raffles")
    Call<RafflesListDTO> getRafflesList();

    @GET("raffles/")
    Call<RafflesListDTO> getOneRaffle(@Query("idRaffle") String id);

    @GET("clients/raffles")
    Call<RafflesListDTO> getMyRafflesList(@Header("x-access-token") String token);
}
