package com.example.augardevent.Service.Provider;

import android.net.sip.SipSession;

import com.example.augardevent.Model.DTO.RafflesDTO;
import com.example.augardevent.Model.DTO.RafflesListDTO;
import com.example.augardevent.Service.Interface.RafflesService;
import com.example.augardevent.SharedPreferences.SharedPref;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkProvider {

    private static Retrofit retrofit = null;
    private static NetworkProvider instance;
    private static RafflesService rafflesService;
    private final static String mockAPI = "https://demo6576625.mockable.io/api/";
    private final static String prodBaseUrl = "https://10.0.2.2/api/";

    public static NetworkProvider getInstance() {
        if (instance == null) {
            instance = new NetworkProvider();
        }
        return instance;
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(mockAPI)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    private NetworkProvider() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(mockAPI)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        rafflesService = retrofit.create(RafflesService.class);
    }

    public void getRafflesList(Listener<List<RafflesDTO>> listener) {
        rafflesService.getRafflesList().enqueue(new Callback<RafflesListDTO>() {
            @Override
            public void onResponse(Call<RafflesListDTO> call, Response<RafflesListDTO> response) {
                if(response.isSuccessful()) {
                    RafflesListDTO rafflesListDTO = response.body();

                    listener.onSuccess(rafflesListDTO.getRafflesDTOList());
                }
            }

            @Override
            public void onFailure(Call<RafflesListDTO> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public void getMyRafflesList(Listener<List<RafflesDTO>> listener) {
        rafflesService.getMyRafflesList(SharedPref.getToken()).enqueue(new Callback<RafflesListDTO>() {
            @Override
            public void onResponse(Call<RafflesListDTO> call, Response<RafflesListDTO> response) {
                if(response.isSuccessful()) {
                    RafflesListDTO rafflesListDTO = response.body();
                    listener.onSuccess(rafflesListDTO.getRafflesDTOList());
                }
            }

            @Override
            public void onFailure(Call<RafflesListDTO> call, Throwable t) {
                listener.onError(t);
            }
        });
    }

    public interface Listener<T> {
        void onSuccess(T data);

        void onError(Throwable t);
    }
}
