package com.example.augardevent.Adapter;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.augardevent.Model.DTO.RafflesDTO;
import com.example.augardevent.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RaffleAdapter extends RecyclerView.Adapter<RaffleAdapter.RafflesViewHolder> {

    private List<RafflesDTO> rafflesDTOList;
    private ItemClickListener itemClickListener;

    public void setRafflesList(List<RafflesDTO> rafflesList) {
        this.rafflesDTOList = rafflesList;
        notifyDataSetChanged();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RafflesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raffles_item, viewGroup, false);
        return new RafflesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RafflesViewHolder holder, int position) {
        RafflesDTO rafflesDTO = rafflesDTOList.get(position);
        holder.raffleName.setText(rafflesDTO.getWatchDTO().getName());
        Glide.with(holder.itemView).load(rafflesDTO.getWatchDTO().getPictureDTOArrayList().get(0).getUrl()).into(holder.raffleImage);
        holder.rafflePrice.setText(rafflesDTO.getWatchDTO().getPrice().toString());
        new CountDownTimer(Integer.valueOf(rafflesDTO.getEndTime()), 1000) {

            public void onTick(long millisUntilFinished) {
                holder.endDateTime.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                holder.endDateTime.setText("Done!");
            }
        }.start();

        if (itemClickListener != null) {
            holder.itemView.setOnClickListener(v -> itemClickListener.onclick(rafflesDTO));
        }
    }

    @Override
    public int getItemCount() {
        return rafflesDTOList != null ? rafflesDTOList.size() : 0;
    }

    public interface ItemClickListener {
        void onclick(RafflesDTO rafflesDTO);
    }

    class RafflesViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.raffles_item_image_view)
        ImageView raffleImage;
        @BindView(R.id.raffles_item_name_text_view)
        TextView raffleName;
        @BindView(R.id.raffles_item_end_datetime_text_view)
        TextView endDateTime;
        @BindView(R.id.raffles_item_price_text_view)
        TextView rafflePrice;

        public RafflesViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
